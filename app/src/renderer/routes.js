export default [
  {
    path: '/',
    component: require('components/MainPage'),
    redirect: '/game/config',
    children: [{
      path: '/config/:listCode/:listName/:withTags',
      component: require('components/ConfigPage'),
      props: true
    }, {
      path: '/game/config',
      component: require('components/game/ConfigPage'),
    }, {
      path: '/game/play',
      component: require('components/game/PlayPage'),
    }, {
      path: '/ranking',
      component: require('components/RankingPage')
    }]
  }
]
