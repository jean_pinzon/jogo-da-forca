# jogo-da-forca

## Requirements

- Node version >= v7.9.0
- Git

## How to run

``` bash
git clone https://bitbucket.org/jean_pinzon/jogo-da-forca
cd jogo-da-forca
npm install
npm run dev
```
